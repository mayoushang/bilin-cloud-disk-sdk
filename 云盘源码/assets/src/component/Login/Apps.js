import React, { useCallback, useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

function Apps() {

    const JumpUrl = () =>{
        const history = useHistory();
        const Days = 365;
        const exp = new Date(); 
        exp.setTime(exp.getTime() + Days*24*60*60*1000);
        document.cookie = "thisapp=ok; expires="+exp.toGMTString()+"; path=/";
        history.push("/home");
    }

    return (
        <div>{JumpUrl()}</div>
    );
}

export default Apps;
