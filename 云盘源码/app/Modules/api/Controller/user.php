<?php
if(!defined('APP_VER')) exit("die!");
class user extends App
{	
	function __construct(){
		parent::__construct();
	}

	//获取用户信息
	public function getUserInfo(){
		$lib_fun = $this->mModel("lib_fun","",Modules);
		$lib_user = $this->mModel("lib_user");
		$user_id = $lib_fun->getUserId($this->mArgs("token"));
		$userInfo = $lib_user->getUserInfo($user_id);
		if ($userInfo['status']!=='0') $lib_fun->parError(-2,'账号被封禁');
		$lib_fun->parError(0,'获取成功',$userInfo);
	}

	//api账号注册
	public function register(){
		$lib_user = $this->mModel("lib_user");
		$user['userName'] = $this->mArgs("userName");
		$user['Password'] = $this->mArgs("Password");
		$user['randstr'] = $this->mArgs("randstr");
		$user['ticket'] = $this->mArgs("ticket");
		$user = $lib_user->register($user);
		exit($user);
	}

	//api账号登陆
	public function login(){
		$lib_user = $this->mModel("lib_user");
		$user['userName'] = $this->mArgs("userName");
		$user['Password'] = $this->mArgs("Password");
		$user['randstr'] = $this->mArgs("randstr");
		$user['ticket'] = $this->mArgs("ticket");
		$user = $lib_user->login($user);
		exit($user);
	}

	//webdav获取用户token
	public function getToken(){
		$lib_fun = $this->mModel("lib_fun","",Modules);
		$lib_user = $this->mModel("lib_user");

		$secret_key = $this->mArgs("secret_key");
		$user_name = $this->mArgs("user_name");
		$user_pass = $this->mArgs("user_pass");
		if (empty($secret_key) || empty($user_name) || empty($user_pass)) $lib_fun->parError(1,'参数错误');
		$token = $lib_user->getToken($user_name,$user_pass,$secret_key);
		if (empty($token)) $lib_fun->parError(2,'账号或密码错误');
		$lib_fun->parError(0,'获取成功',['token'=>$token]);
	}
}
?>