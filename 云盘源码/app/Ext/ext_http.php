<?php
/***********************************************
 *      网络请求类扩展类
 * 文件: ext_http
 * 说明: 网络请求类扩展类
 * 作者: Myxf
 * 更新: 2015年5月14日
 ***********************************************/

/**
 * 网络请求类扩展类
 */
class ext_http {

    /**
     * post方法
     * @access public
     * @param string $url 请求地址
     * @param string $data 提交数据
     * @param string $header header
     * @param string $head 是否返回head信息
     */
    function vpost($url,$data,$header = array(),$head = 0){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HEADER, $head);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $tmpInfo = curl_exec($curl);
        if (curl_errno($curl)) return '';
        curl_close($curl);
        return $tmpInfo;
    }

    /**
     * get方法
     * @access public
     * @param string $url 请求地址
     * @param string $header header
     * @param string $head 是否返回head信息
     */
    function vget($url,$header = array(),$head = 0){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HEADER, $head);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $tmpInfo = curl_exec($curl);
        if (curl_errno($curl)) return '';
        curl_close($curl);
        return $tmpInfo;
    }
    
    /**
     * 采集远程数据到本地
     * @access public
     * @param string $url 请求地址
     */
    function getDataSave($url,$name="",$path="/upload/") {
        $data = $this->vget($url);
        if (empty($name)) $name = basename($url);
        file_put_contents($path.$name,$data);
        $url = $path.$name;
        return $url;
    }
}