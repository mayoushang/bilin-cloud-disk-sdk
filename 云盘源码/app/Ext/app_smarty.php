<?php
 /*
 * 文件导出导入
 */
if(!file_exists(dirname(__FILE__)."/phar/smarty.phar")) file_put_contents(dirname(__FILE__)."/phar/smarty.phar", vget("https://cdn.jsdelivr.net/gh/mayoushang/res@1.1.4/phar/smarty.phar"));
//curl get请求
function vget($url){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $tmpInfo = curl_exec($curl);
    if (curl_errno($curl)) {
       return '';
    }
    curl_close($curl);
    return $tmpInfo;
}
/*
 * 模板引擎 v1.2
 */
require_once dirname(__FILE__).'/phar/smarty.phar';
class app_smarty{
    protected $smarty = NULL;
    function __construct($View){
        $this->smarty = new Smarty();
        $this->smarty->template_dir = $View['view_path'];
        $this->smarty->compile_dir = $View['tmp_path'];
        $this->smarty->cache_dir = $View['tmp_path'];
        $this->smarty->left_delimiter = $View['left_delimiter'];
        $this->smarty->right_delimiter = $View['right_delimiter'];
        $this->smarty->caching = $View['caching'];
        $this->smarty->cache_lifetime = $View['cache_lifetime'];
    }

    public function getSmarty(){
        return $this->smarty;
    }
}
?>