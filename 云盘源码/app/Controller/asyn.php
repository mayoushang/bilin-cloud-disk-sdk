<?php
header("Content-type: application/json; charset=utf-8");
set_time_limit(0);
ini_set('memory_limit', '128M');
require(dirname(__FILE__)."/../app.php");
$App = new App();
$ext_http = $App->mClass("ext_http");
$ext_mqtt = $App->mClass("ext_mqtt");
$lib_game = $App->mModel("lib_game");

$action = empty($argv) ? "index" : $argv[1];

//异步开奖
if($action=="asyn_game"){ // /www/server/php/56/bin/php /www/wwwroot/www.qtings.com/api/asyn.php asyn_game
    $is_process = checkprocess("asyn.php ".$action);
    if ($is_process) exit("is running!");
    while (true) {
        sleep(30);
        $M = $App->mModel("lib_db");
        $M->db("game_duobao_issue");
        $issue = $M->getAll("id,sid,iid,num_a","num_a<>'' and num_b=''");
        foreach ($issue as $key => $one) {
            $M->db("game_duobao");
            $duobao = $M->getOne("id,need_num,gift_id,`type`","id=".$one['sid']);
            //$backdata = $ext_http->vget("https://api.apiose122.com/CQShiCai/getBaseCQShiCai.do?issue=&lotCode=10036",array("User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36"));
            $backdata['result']['data']['preDrawCode'] = mt_rand(11111,99999);
            if ($backdata){
                //$backdata = json_decode($backdata,true);
                $num_all = $duobao['need_num'];
                $num_a = $one['num_a'];
                $num_b = str_replace(",", "", @$backdata['result']['data']['preDrawCode']);
                if ($num_b>0){
                    $num = (($num_a + $num_b) % $num_all) + 1000001;

                    //根据幸运号找用户id
                    $M->db("game_duobao_log");
                    $duobao_log = $M->getOne("id,user_id","sid=".$one['sid']." and iid=".$one['iid']." and num=".$num);
                    $user_id = intval($duobao_log['user_id']);

                    //更新本期奖池
                    $M->db("game_duobao_issue");
                    $update = array();
                    $update['num_b'] = $num_b;
                    $update['num'] = $num;
                    $update['user_id'] = $user_id;
                    $update['utime'] = time();
                    $M->edit($update,"id=".$one['id']);

                    //生成下期奖池
                    $update = array();
                    $update['sid'] = $one['sid'];
                    $update['iid'] = $one['iid'] + 1;
                    $update['ctime'] = time();
                    $M->add($update);

                    //更新下期奖池
                    $M->db("game_duobao");
                    $update = array();
                    $update['lssue'] = $one['iid'] + 1;
                    $M->edit($update,"id=".$duobao['id']);

                    //给用户发送奖品
                    if ($duobao['type']=='gift'){ //礼物奖品
                        $lib_game->send_gift($duobao['gift_id'],1,$user_id,"奖励礼物");
                    }
                    if ($duobao['type']=='card'){ //卡密奖品
                        $lib_game->send_card($duobao['gift_id'],$user_id,$duobao_log['id']);
                    }

                    //通知客户端刷新
                    $text['asyn_id'] = $duobao['id'];
                    $text['asyn_type'] = 'refresh';
                    $data = array("action"=>"duobao","text"=>$text);
                    $ext_mqtt->serverPush($data);
                }
            }
        }
        $M->close();
    }
    echo "start running!"; 
}

function checkprocess($name)
{   
    $cmd = 'ps axu|grep "'.$name.'"|grep -v "grep"|wc -l';
    $ret = shell_exec("$cmd");
    $ret = rtrim($ret, "\r\n");
    if($ret === "0" || $ret === "1"){
        return false;
    }
    return true;
}
?>