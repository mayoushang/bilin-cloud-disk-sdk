<?php
if(!defined('APP_VER')) exit("die!");
class api extends App
{	
	function __construct(){
		parent::__construct();
		$this->lib_api = $this->mModel("lib_api");
	}

	//api获取外链地址
	public function getDownLoadUrl(){
		header("content-type:application/json");
		$this->lib_api->ckAuth($this->mArgs("secret_key"));
		$M = $this->mModel("lib_db");
		$ext_fun = $this->mClass("ext_fun");

		$user_id = $_SESSION['user_id'];
		$Path = urldecode($this->mArgs("filePath"));
		$filePath = explode("/", $Path);
		if (count($filePath)==2){ //如果是根目录
			$fileName = $filePath[1];
			$M->db("files");
			$files = $M->getOne("id,size","`name`='".$fileName."' and user_id=".$user_id);
		}else{
			$fileName = end($filePath);
			$Path = str_replace($fileName, "", $Path);
			$M->db("files");
			$files = $M->getOne("id,size","`name`='".$fileName."' and source_name like '%uploads/".$user_id.$Path."%' and user_id=".$user_id);
		}
		if (!$files) exit(json_encode(['code'=>1,'msg'=>'文件未找到']));
		$hashids = $this->lib_api->hashids_encode($files['id']);
		$sign = $ext_fun->toacii("UrlSign:".$hashids);
		$url = "/api/v3/file/sourcejump/".$hashids."/".$sign;
		exit(json_encode(['code'=>0,'msg'=>'外链获取成功','backdata'=>['file_name'=>$fileName,'file_size'=>$files['size'],'url'=>$url]]));
	}

	//检测通信
	public function checkLink(){
		header("content-type:application/json");

		$return['msg'] = "操作成功";
		$return['code'] = 0;
		$return['data'] = false;
		exit(json_encode($return));
	}

	//获取业务域名
	public function cnameLink(){
		header("content-type:application/json");

		$return['msg'] = "操作成功";
		$return['code'] = 0;
		$return['data'] = $_SERVER['SERVER_NAME'];
		exit(json_encode($return));
	}

	//是否是会员
	public function isVIPGorup(){
		header("content-type:application/json");
		$lib_user = $this->mModel("lib_user");
		$user_id = $this->user_id;
		$user = $lib_user->getUserInfo($user_id);
		$group_expires = empty($user['group_expires']) ? 0 : $user['group_expires'];
		if (strtotime($group_expires)<time()){
			$isVIPGorup = false;
		}else{
			$isVIPGorup = true;
		}

		$return['msg'] = "操作成功";
		$return['code'] = 0;
		$return['data'] = $isVIPGorup;
		exit(json_encode($return));
	}

	//会员到期时间
	public function expire(){
		header("content-type:application/json");
		$lib_user = $this->mModel("lib_user");
		$user_id = $this->user_id;
		$user = $lib_user->getUserInfo($user_id);
		$group_expires = empty($user['group_expires']) ? 0 : $user['group_expires'];
		if (strtotime($group_expires)<time()){
			$expire = null;
		}else{
			$expire = $group_expires;
		}

		$return['msg'] = null;
		$return['code'] = 0;
		$return['data'] = $expire;
		exit(json_encode($return));
	}

	public function interval(){
		header("content-type:application/json");

		$return['msg'] = "操作成功";
		$return['code'] = 0;
		$return['data'] = 60000;
		exit(json_encode($return));
	}

	//内容审查
    public function safeck(){
    	$http = $this->mClass("ext_http");
    	$key = $this->mArgs("key");
    	$context = empty($_POST['context']) ? "" : urldecode($_POST['context']);
    	if ($key!="1w65s9213w1") exit("err");
        if (!empty($context)){
            $backdata = $http->vpost("https://ai.baidu.com/aidemo","content=".$context."&type=textcensor&apiType=censor&requestTime=1627977267142&token=dff54021a3");
            $backdata = @json_decode($backdata,true);
            if (intval($backdata['data']['result']['spam'])!=0){
            	$badword = [];
                foreach ($backdata['data']['result']['reject'] as $key => $one) {
                	if (!empty($one['hit'][0])) $badword[] = $one['hit'][0];
                }
                if (!empty($badword)) $context = str_replace($badword,"~",$context);
            }
        }
        echo $context;
    }
}
?>