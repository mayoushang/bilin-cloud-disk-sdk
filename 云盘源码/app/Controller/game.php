<?php
if(!defined('APP_VER')) exit("die!");
class game extends App
{	
	function __construct(){
		parent::__construct();
		$lib_api = $this->mModel("lib_api");
		$lib_api->ckLogin();
	}

	//夺宝达人首页
	public function duobao(){
		$lib_user = $this->mModel("lib_user");
		$lib_game = $this->mModel("lib_game");
		$user_id = $this->user_id;
		$user = $lib_user->getUserInfo($user_id);
		$action = empty($this->mArgs("action")) ? "free" : $this->mArgs("action");
		if ($action == "free") $_where = "price=0 and ";
		if ($action == "newuser") $_where = "price>0 and need_num<=100 and ";
		$duobao = $lib_game->getDuobaoList(0,$_where);
		

		$this->user = $user;
		$this->lists = $duobao;
	    $this->title = "夺宝达人";
	    $this->action = $action;
	    $this->display("game/duobao_index.html");
	}

	//夺宝达人详情
	public function duobao_view(){
		$lib_user = $this->mModel("lib_user");
		$lib_game = $this->mModel("lib_game");
		$ext_mqtt = $this->mClass("ext_mqtt");
		$user_id = $this->user_id;
		$id = intval($_GET['id']);
		if (empty($id)) $this->mAlert("奖池参数错误",null,null,false);
		
		$user = $lib_user->getUserInfo($user_id);
		$duobao = $lib_game->getDuobaoList($id);
		$user_list = $lib_game->getDuobaoUserList($id,intval($duobao['lssue']));

		$ext_mqtt = $ext_mqtt->getMqtt($user_id);
		//往期数据
		$previous = $lib_game->getPrevious($id);

		$this->user = $user;
		$this->duobao = $duobao;
		$this->user_list = $user_list;
		$this->previous = $previous;
		$this->mq = $ext_mqtt;
	    $this->title = "夺宝达人-".$duobao['name'];
	    $this->display("game/duobao_view.html");
	}

	//夺宝达人购买
	public function duobao_buy(){
		$M = $this->mModel("lib_db");
		$lib_user = $this->mModel("lib_user");
		$lib_game = $this->mModel("lib_game");
		$ext_mqtt = $this->mClass("ext_mqtt");

		//防止并发
		$M->mysql_start();
		$M->sql("select * from ".$M->prefix."lock where id=1 for update");

		$id = intval($_POST['id']);
		$num = empty(intval($_POST['price'])) ? 1 : intval($_POST['price']);
		$user_id = $this->user_id;

		$user = $lib_user->getUserInfo($user_id);
		$M->db("game_duobao");
		$duobao = $M->getOne("id,name,lssue,need_num,max_num,max_lssue,price","id=".$id);
		$need_now = $lib_game->duobao_log_count($id,intval($duobao['lssue']));
		$my_num = $lib_game->duobao_log_count($id,intval($duobao['lssue']),$user_id);
		$price = round($num * $duobao['price'],2);

		if(empty($user_id)){
			$return['code'] = 1;
			$return['msg'] = "请先登录";
		}elseif (!$user){
			$return['code'] = 2;
			$return['msg'] = "用户不存在";
		}elseif($user['score']<$price){
			$return['code'] = 3;
			$return['msg'] = "积分不足";
		}elseif(empty($duobao)){
			$return['code'] = 4;
			$return['msg'] = "奖池不存在";
		}elseif($duobao['need_num']==$need_now){
			$return['code'] = 5;
			$return['msg'] = "等待开奖中";
		}elseif($num>($duobao['need_num']-$need_now)) {
			$return['code'] = 6;
			$return['msg'] = "最多投注".($duobao['need_num']-$need_now)."个";
		}elseif(!empty($duobao['max_num']) && $my_num>=$duobao['max_num']){
			$return['code'] = 6;
			$return['msg'] = "每场限投注".$duobao['max_num']."次";
		}elseif(!empty($duobao['max_num']) && $num>$duobao['max_num']){
			$return['code'] = 6;
			$return['msg'] = "每次限投注".$duobao['max_num']."个";
		}elseif(!empty($duobao['max_lssue']) && $duobao['lssue']>$duobao['max_lssue']){
			$return['code'] = 7;
			$return['msg'] = "库存不足，正在补货中";
		}else{
			if ($price>0){
				$res = $lib_user->money_log($user_id,"-".$price,$user['score'],"夺宝达人","参与".$duobao['name']."第".$duobao['lssue']."期夺宝达人，花费".$price."积分。");
			}else{
				$res = true;
			}
			
			if (!$res){
				$return['code'] = 8;
				$return['msg'] = "扣除积分失败，请重试";
			}else{
				$lib_game->duobao_buy($id,$duobao['lssue'],$num,$need_now,$user_id);
				$return['code'] = 0;
				$return['msg'] = "投注成功";
			}
		}
		$M->mysql_commit();

		if ($return['code']==0){
			//是否可以开出数值A
			$num_a = 0;
			if ($duobao['need_num'] - ($need_now + $num)==0) $num_a = $lib_game->duobao_num_a($id,$duobao['lssue']); //生成数值A
			//推送mqtt消息
			$text['asyn_id'] = $duobao['id'];
			$text['asyn_type'] = 'update';
			$text['asyn_lssue'] = intval($duobao['lssue']);
			$text['asyn_need'] = $duobao['need_num'] - ($need_now + $num);
			$text['asyn_progress'] = round(($need_now + $num) / $duobao['need_num'] * 100,2);
			$text['asyn_num_a'] = intval($num_a);

			$data = array("action"=>"duobao","text"=>$text);
			$ext_mqtt->serverPush($data);
		}
		exit(json_encode($return));
	}

	//投注记录
	public function duobao_log(){
		$M = $this->mModel("lib_db");
		$lib_user = $this->mModel("lib_user");
		$lib_game = $this->mModel("lib_game");

		$user_id = $this->user_id;
		$limit = 10;
		$page = empty($_GET['page']) ? 1 : intval($_GET['page']);
		$state = empty($_GET['state']) ? "" : intval($_GET['state']);
		$where = "user_id=".$user_id;
		if ($state==1) $where .= " and (select count(*) from ".$M->prefix."game_duobao_issue where num=".$M->prefix."game_duobao_log.num and sid=".$M->prefix."game_duobao_log.sid and iid=".$M->prefix."game_duobao_log.iid)>0";
		$M->db("game_duobao_log");
		if ($state==2) $where .= " and (select count(*) from ".$M->prefix."game_duobao_issue where num<>".$M->prefix."game_duobao_log.num and sid=".$M->prefix."game_duobao_log.sid and iid=".$M->prefix."game_duobao_log.iid)>0";
		$M->db("game_duobao_log");
		$pager = $M->pager("sid,iid,user_id,num,ctime,mark",$where." group by iid,sid,id",$page,$limit,"id desc");
		$log = $pager['list'];
		foreach ($log as $key => $one) {
			$duobao = $lib_game->getDuobaoList($one['sid'],"",1);
			$iisue = $lib_game->duobao_issue($one['sid'],$one['iid']);
			$log[$key]['gift_name'] = $duobao['name'];
			$log[$key]['gift_price'] = $duobao['gift_price'];
			$log[$key]['gift_pic'] = $duobao['gift_pic'];
			$log[$key]['ctime'] = date('m-d H:i:s',$one['ctime']);
			if ($iisue['user_id']==0) $log[$key]['state'] = 0;
			if ($iisue['user_id']>0 && $iisue['num']==$one['num']) $log[$key]['state'] = 2;
			if ($iisue['user_id']>0 && $iisue['num']!=$one['num']) $log[$key]['state'] = 1;
		}

		$this->state = $state;
		$this->log = $log;
		$this->pager = $pager;
		$this->title = "夺宝记录";
	    $this->display("game/duobao_log.html");
	}

	//获取参与记录
	public function duobao_userlist(){
		$lib_game = $this->mModel("lib_game");
		$id = empty($_POST['id']) ? 1 : intval($_POST['id']);
		$iid = empty($_POST['iid']) ? 1 : intval($_POST['iid']);
		$page = empty($_POST['page']) ? 1 : intval($_POST['page']);
		$page = ($page - 1) * 10;
		$user_list = $lib_game->getDuobaoUserList($id,$iid,$page);
		if ($user_list){
			$return['code'] = 0;
			$return['msg'] = "获取成功";
			$return['lists'] = $user_list;
		}else{
			$return['code'] = 1;
			$return['msg'] = "没有更多啦";
		}
		exit(json_encode($return));
	}

}
?>