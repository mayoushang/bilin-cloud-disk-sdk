<?php
if(!defined('APP_VER')) exit("die!");
class user extends App
{	
	function __construct(){
		parent::__construct();
		$lib_api = $this->mModel("lib_api");
		$lib_api->ckLogin();
	}

	//账号主页
	public function account(){
		$M = $this->mModel("lib_db");
		$lib_user = $this->mModel("lib_user");
		$user_id = $this->user_id;
		$user = $lib_user->getUserInfo($user_id);

		$limit = 10;
	    $page = empty($this->mArgs("page")) ? 1 : intval($this->mArgs("page"));

	    $M->db("money_log");
	    $pager = $M->pager("*","user_id=".$user_id,$page,$limit);
	    $log = $pager['list'];
	    foreach ($log as $key => $one) {
	        $log[$key]['ctime'] = date('m-d H:i:s',$one['ctime']);
	    }

	    $M->db("cash");
	    $tx_log = $M->getAll("ctime,money,amount,state,alipay_card","user_id=".$user_id,"id desc",10);
	    $tx_money = $M->getSum("money","user_id=".$user_id);
	    foreach ($tx_log as $key => $one) {
	    	$tx_log[$key]['ctime'] = date("Y-m-d",$one['ctime']);
	    	$tx_log[$key]['state'] = empty($one['state']) ? "处理中" : (($one['state']==1) ? "已完成" : "被退回");
	    	$tx_log[$key]['amount'] = $one['amount'] * 10;
	    }

	    $M->db("users");
	    $invite = $M->getCount("aid=".$user_id);

		$this->page = ['title'=>'财务管理','pan_path'=>pan_path];
		$this->user = $user;
		$this->log = $log;
		$this->pager = $pager;
		$this->tx_log = $tx_log;
		$this->tx_money = $tx_money;
		$this->invite = $invite;
		$this->display("user_account.html");
	}

	//获取下级用户
	public function user_list(){
		$M = $this->mModel("lib_db");
		$user_id = $this->user_id;

		$limit = 10;
	    $page = empty($this->mArgs("page")) ? 1 : intval($this->mArgs("page"));
	    $M->db("users");
	    $pager = $M->pager("id,created_at,nick","aid=".$user_id,$page,$limit);
	    if (!$pager['list']) exit(json_encode(['code'=>1]));
	    $list = $pager['list'];
	    $return = ['code'=>0,'list'=>$list,'num'=>$pager['total']];
	    exit(json_encode($return));
	}

	//申请提现
	public function withdrawal(){
		$M = $this->mModel("lib_db");
		$user_id = $this->user_id;
		$lib_user = $this->mModel("lib_user");
		$user = $lib_user->getUserInfo($user_id);

		$money = intval($this->mArgs("money"));
		$alipay_card = $this->mArgs("alipay_card");
		$alipay_name = $this->mArgs("alipay_name");
		if ($money<100){
			$return['code'] = 1;
			$return['msg'] = "提现积分不能小于100";
		}elseif($user['status']!=0){
			$return['code'] = 2;
			$return['msg'] = "账号已被封禁";
		}elseif(empty($alipay_card) || empty($alipay_name)){
			$return['code'] = 3;
			$return['msg'] = "支付宝账号或者真实姓名不能为空";
		}elseif($user['score']<$money){
			$return['code'] = 4;
			$return['msg'] = "积分余额不足";
		}else{
			if(empty($user['alipay_card'])){
				$M->db("users");
				$update = array();
				$update['alipay_card'] = $user['alipay_card'] = $alipay_card;
				$update['alipay_name'] = $user['alipay_name'] =$alipay_name;
				$M->edit($update,"id=".$user_id);
			}

			$state = $lib_user->money_log($user_id,"-".$money,$user['score'],"积分提现","积分提现至支付宝账号".$user['alipay_card']);
			if($state){

				//写日志
				$M->db("cash");
				$update = array();
				$update['user_id'] = $user_id;
				$update['money'] = round($money / 10 * 0.95,2);
				$update['amount'] = $money / 10;
				$update['ctime'] = time();
				$update['utime'] = time();
				$update['alipay_card'] = $user['alipay_card'];
				$update['alipay_name'] = $user['alipay_name'];
				$M->add($update);

				$return['code'] = 0;
				$return['msg'] = "提现申请成功";
			}else{
				$return['code'] = 4;
				$return['msg'] = "处理错误，请重试";
			}
		}
		exit(json_encode($return));
	}
}
?>