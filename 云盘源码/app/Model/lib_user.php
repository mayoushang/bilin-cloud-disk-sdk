<?php
class lib_user extends mModel
{
	//账号注册
	public function register($user = array()){
		$http = $this->mClass("ext_http");
		return $http->vpost(pan_path."/api/v3/user",json_encode($user));
	}

	//账号登陆
	public function login($user = array()){
		$ext_fun = $this->mClass("ext_fun");
		$lib_api = $this->mModel("lib_api");
		$http = $this->mClass("ext_http");

		$user = $http->vpost(pan_path."/api/v3/user/session",json_encode($user));
		$user = json_decode($user,true);
		if (!empty($user['data']['id'])){
			$token = array();
			$token['user_id'] = $lib_api->hashids_decode($user['data']['id'],1)[0];
			$token['hashid'] = $user['data']['id'];
			$token = $ext_fun->toacii($token);
			$user['data']['token'] = $token;
		}
		return json_encode($user);
	}

	//webdav获取token
	public function getToken($username,$password,$secret_key){
		$lib_api = $this->mModel("lib_api");
		$http = $this->mClass("ext_http");
		$M = $this->mModel("lib_db");

		$data = array();
		$data['userName'] = $username;
		$data['Password'] = $password;
		$data['randstr'] = "";
		$data['ticket'] = "";
		$user = $this->login($data);
		$user = json_decode($user,true);
		if (empty($user['data']['id'])) return "";
		$user_id = $lib_api->hashids_decode($user['data']['id'],1)[0];
		if (empty($user_id) || empty($user['data']['group']['webdav'])) return "";
		$M->db("webdavs");
		$webdavs = $M->getOne("user_id","user_id=".$user_id." and deleted_at is null and password='".$secret_key."'");
		if (!$webdavs) return "";

		$backdata = $http->vpost(pan_path."/api/v3/user/session",json_encode($data),[],1);
		preg_match('/Set-Cookie: cloudreve-session=(.*);/iU',$backdata,$str);
		$sessionId = $str[1];
		return $sessionId;
	}

	//获取用户头像地址
	public function getAravatar($user_id = 0){
		$M = $this->mModel("lib_db");
        $lib_api = $this->mModel("lib_api");
		$M->db("users");
		$user = $M->getOne("avatar","id=".$user_id);
		if (empty($user['avatar']) || $user['avatar']=="gravatar") return "https://gravatar.loli.net/avatar/a1f7c794e110b025b7a9a0e5cd69f626?d=mm&s=200";
		return pan_path."/api/v3/user/avatar/".$lib_api->hashids_encode($user_id,1)."/l";
	}

	//获取用户基本信息
	public function getUserInfo($user_id = 0){
		$M = $this->mModel("lib_db");
        $lib_api = $this->mModel("lib_api");
		$M->db("users");
		$user = $M->getOne("*","id=".$user_id);
		$user['avatar'] = $this->getAravatar($user_id);
		$user['hashid'] = !empty($user_id) ? $lib_api->hashids_encode($user_id,1) : "";
		return $user;
	}

	//积分变动
	public function money_log($user_id,$money,$money_old,$title,$mark,$order_id=""){
        $M = new lib_db();
        $M->db("users");
        $update = array();
        $update['score'] = $money_old + $money;
        $state = $M->edit($update,"id=".$user_id." and score=".$money_old);
        if ($state){
            $M->db("money_log");
            $update = array();
            $update['title'] = $title;
            $update['mark'] = $mark;
            $update['money'] = $money;
            $update['money_old'] = $money_old;
            $update['ctime'] = time();
            $update['user_id'] = $user_id;
            $update['order_id'] = $order_id;
            $M->add($update);
            return true;
        }else{
            return false;
        }
    }
}