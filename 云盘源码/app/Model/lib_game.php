<?php
class lib_game extends mModel
{
    //获取夺宝列表
    public function getDuobaoList($sid = 0,$where = "",$state = 0){
        $M = $this->mModel("lib_db");
        $M->db("game_duobao");
        $where .= " 1=1";
        if ($state==0) $where .= " and state=0";
        if (!empty($sid)) $where .= " and id=".$sid;
        $duobao = $M->getAll("*",$where,"sort asc");
        foreach ($duobao as $key => $one) {
            $duobao_log_count = $this->duobao_log_count($one['id'],$one['lssue']);
            $duobao[$key]['need'] = $one['need_num'] - $duobao_log_count;
            $duobao[$key]['progress'] = round($duobao_log_count / $one['need_num'] * 100,2);
            $M->db("game_duobao_issue");
            $issue = $M->getOne("num_a","sid=".$one['id']." and iid=".$one['lssue']);
            $duobao[$key]['num_a'] = $issue['num_a'];
            $lastissue = $this->duobao_issue($one['id'],$one['lssue']-1);
            $duobao[$key]['lastissue_userid'] = $lastissue['user_id'];
            $duobao[$key]['lastissue_pic'] = $lastissue['pic'];
            $duobao[$key]['lastissue_username'] = $lastissue['username'];
            $duobao[$key]['lastissue_num'] = $lastissue['num'];
        }
        if (!empty($sid)) return @$duobao[0];
        return $duobao;
    }

    //夺宝已下注数量
    public function duobao_log_count($sid,$iid,$user_id = 0){
        $ext_fun = $this->mClass("ext_fun");
        $M = $this->mModel("lib_db");
        $M->db("game_duobao_log");
        $where = "sid=".$sid." and iid=".$iid;
        if (!empty($user_id)){
            $ip = $ext_fun->getIP();
            if (!empty($ip)){
                $where .= " and (user_id=".$user_id." or ip='".$ip."')";
            }else{
                $where .= " and user_id=".$user_id;
            }
        }
        $log = $M->getCount($where);
        return intval($log);
    }

    //上期夺宝中奖用户信息
    public function duobao_issue($sid,$iid=0,$page=1,$limit=20){
        $M = $this->mModel("lib_db");
        $lib_user = $this->mModel("lib_user");
        $M->db("game_duobao_issue");
        $page = ($page - 1) * $limit;
        $where = "user_id>0 and sid=".$sid;
        if (!empty($iid)) $where .= " and iid=".$iid;
        $issue = $M->getAll("*",$where,"id desc",$page.",".$limit);
        foreach ($issue as $key => $one) {
            $user = $lib_user->getUserInfo($one['user_id']);
            $issue[$key]['pic'] = $user['avatar'];
            $issue[$key]['username'] = $user['nick'];
        }
        if (!empty($iid)) return !empty($issue[0]) ? $issue[0] : ['user_id'=>0,'pic'=>'','username'=>'','num'=>''];
        return !empty($issue) ? $issue : ['user_id'=>0,'pic'=>'','username'=>'','num'=>''];
    }

    //获取投注记录
    public function getDuobaoUserList($sid,$iid,$page = 0){
        $M = $this->mModel("lib_db");
        $lib_user = $this->mModel("lib_user");
        $M->db("game_duobao_log");
        $log = $M->getAll("num,user_id,ctime","sid=".$sid." and iid=".$iid,"id asc",$page.",10");
        foreach ($log as $key => $one) {
            $user = $lib_user->getUserInfo($one['user_id']);
            $log[$key]['pic'] = $user['avatar'];
            $log[$key]['username'] = $user['nick'];
            $log[$key]['ctime'] = date('m-d H:i:s',$one['ctime']);
        }
        return $log;
    }

    //获取往期数据
    public function getPrevious($sid){
        $M = $this->mModel("lib_db");
        $lib_user = $this->mModel("lib_user");
        $M->db("game_duobao_issue");
        $issue = $M->getAll("id,iid,num,user_id,num_a,num_b,utime","sid=".$sid." and user_id>0","id desc","30");
        foreach ($issue as $key => $one) {
            $issue[$key]['utime'] = date("m-d H:i:s",$one['utime']);
            $M->db("users");
            $user = $lib_user->getUserInfo($one['user_id']);
            $issue[$key]['pic'] = $user['avatar'];
            $issue[$key]['username'] = $user['nick'];
        }
        return $issue;
    }

    //夺宝下注
    public function duobao_buy($sid,$iid,$tznum,$need_now,$user_id){
        $M = $this->mModel("lib_db");
        $ext_fun = $this->mClass("ext_fun");
        $M->db("game_duobao_issue");
        $issue = $M->getOne("id","sid=".$sid." and iid=".$iid);
        if (empty($issue)){
            $update = array();
            $update['sid'] = $sid;
            $update['iid'] = $iid;
            $update['ctime'] = time();
            $M->add($update);
        }
        $M->db("game_duobao_log");
        $need_now = 1000000 + $need_now;
        $num = "";
        for ($i=1; $i <=$tznum ; $i++) { 
            $num = $need_now + $i;
            $update = array();
            $update['sid'] = $sid;
            $update['iid'] = $iid;
            $update['num'] = $num;
            $update['user_id'] = $user_id;
            $update['ctime'] = time();
            $update['ip'] = $ext_fun->getIP();
            $M->add($update);
        }        
    }

    //生成数值A
    public function duobao_num_a($sid,$iid){
        $M = $this->mModel("lib_db");
        $M->db("game_duobao_log");
        $num_a = $M->getSum("ctime","1=1 order by id desc limit 100");
        if (empty($num_a)) $num_a = time() + mt_rand(111111,999999);
        $num_a = substr($num_a,-6);
        if (substr($num_a, 0,1)=="0") $num_a = "1".substr($num_a, 1,5);
        $M->db("game_duobao_issue");
        $update = array();
        $update['num_a'] = $num_a;
        $M->edit($update,"sid=".$sid." and iid=".$iid);
        return $num_a;
    }

    //发送卡密礼物
    public function send_card($gift_id,$user_id,$id){
        $M = $this->mModel("lib_db");
        $M->db("giftcard");
        $giftcard = $M->getOne("id,card_id,card_pass","user_id=0 and type_id=".$gift_id);
        if ($giftcard){
            $update = array();
            $update['user_id'] = $user_id;
            $update['utime'] = time();
            $M->edit($update,"id=".$giftcard['id']);

            //发送卡密
            $M->db("game_duobao_log");
            $pass = "";
            $update = array();
            if (!empty($giftcard['card_pass'])) $pass = "密码为：".$giftcard['card_pass']."，";
            $update['mark'] = "获奖卡号为：".$giftcard['card_id']."，".$pass."使用方式见游戏详情页。";
            $M->edit($update,"id=".$id);
        }
    }

    //发送礼物
    public function send_gift($gift_id,$num,$user_id,$title){
        $lib_gift = $this->mModel("lib_gift");
        $lib_gift->send_gift($gift_id,$num,$user_id,$title);
    }
}