<?php
class lib_gift extends mModel
{
	//获取礼物列表
    public function getGiftList(){
        $M = $this->mModel("lib_db");
        $M->db("gift");
        $gift = $M->getAll("*","is_show=0","gift_price asc");
        return $gift;
    }

    //购买礼物
    public function bugGift($gift_id,$num,$user_id = 0){
        $M = $this->mModel("lib_db");
        $lib_user = $this->mModel("lib_user");

        $M->db("gift");
        $gift = $M->getOne("id,gift_name,gift_price","id=".$gift_id);
        $money = intval($gift['gift_price']) * $num;
        $M->db("users");
        $user = $M->getOne("id,score","status=0 and id=".$user_id);
        if (empty($money)){
            $return['code'] = 2;
            $return['msg'] = "礼物不存在";
        }elseif(!$user){
            $return['code'] = 3;
            $return['msg'] = "用户被禁用";
        }elseif($user['score']<$money){
            $return['code'] = 4;
            $return['msg'] = "积分不足请充值";
        }else{
            $state = $lib_user->money_log($user_id,"-".$money,$user['score'],"购买礼物","购买礼物".$gift['gift_name']."*".$num."，花费".$money."积分。");
            if (!$state){
                $return['code'] = 5;
                $return['msg'] = "请求错误";
            }else{
                //发送礼物
                $this->send_gift($gift_id,$num,$user_id,"购买礼物");
                $return['code'] = 0;
                $return['msg'] = "购买成功";
            }
        }
        return $return;
    }

    //我的背包
    public function getMyGift($user_id = 0){
        $M = $this->mModel("lib_db");
        $M->db("my_gift");
        $gift = $M->getAll("*","gift_num>0 and user_id=".$user_id);
        foreach ($gift as $key => $one) {
            $M->db("gift");
            $_gift = $M->getOne("gift_name,gift_pic,gift_price","id=".$one['gift_id']);
            $gift[$key]['gift_name'] = $_gift['gift_name'];
            $gift[$key]['gift_pic'] = $_gift['gift_pic'];
            $gift[$key]['gift_price'] = $_gift['gift_price'];
        }
        return $gift;
    }

    //发送礼物
    public function send_gift($gift_id,$num,$user_id,$title,$fid=0){
        $M = $this->mModel("lib_db");
        $M->db("my_gift");
        $gift = $M->getOne("id,gift_num","gift_id=".$gift_id." and user_id=".$user_id);
        if ($gift){
            $update = array();
            $update['gift_num'] = $gift['gift_num'] + $num;
            $M->edit($update,"id=".$gift['id']);
        }else{
            $update = array();
            $update['gift_num'] = $num;
            $update['gift_id'] = $gift_id;
            $update['user_id'] = $user_id;
            $M->add($update);
        }
        $M->db("my_gift_log");
        $update = array();
        $update['title'] = $title;
        $update['gift_id'] = $gift_id;
        $update['gift_num'] = $num;
        $update['ctime'] = time();
        $update['fid'] = $fid;
        $update['user_id'] = $user_id;
        $M->add($update);
    }

    //打赏礼物
    public function sendGift($gift_id,$num,$sid,$user_id = 0){
        $M = $this->mModel("lib_db");
        $lib_user = $this->mModel("lib_user");
        //查询礼物数量
        $M->db("gift");
        $gift = $M->getOne("gift_name,gift_price","id=".$gift_id);
        $M->db("shares");
        $shares = $M->getOne("id,user_id,source_name","id=".$sid);
        $M->db("users");
        $_user = $M->getOne("id,nick","id=".$user_id);
        $user = $M->getOne("id,score","status=0 and id=".intval($shares['user_id']));
        $M->db("my_gift");
        $my_gift = $M->getOne("id,gift_id","gift_num>=".$num." and gift_id=".$gift_id." and user_id=".$user_id);
        if (empty($user_id)){
            $return['code'] = 1;
            $return['msg'] = "请先登录";
        }elseif(!$user){
            $return['code'] = 2;
            $return['msg'] = "用户不存在";
        }elseif (empty($gift_id) || empty($num) || empty($sid)){
            $return['code'] = 3;
            $return['msg'] = "参数不完整";
        }elseif(!$my_gift){
            $return['code'] = 4;
            $return['msg'] = "礼物数量不足，请先购买";
        }elseif(!$shares){
            $return['code'] = 5;
            $return['msg'] = "文件分享不存在";
        }elseif(!$gift){
            $return['code'] = 6;
            $return['msg'] = "礼物不存在";
        }else{
            $this->send_gift($gift_id,"-".$num,$user_id,"打赏礼物",$sid);
            $money = round(($num * $gift['gift_price']) * 0.9,0);
            //增加用户积分
            $lib_user->money_log($shares['user_id'],$money,$user['score'],"获得打赏","分享文件[".$shares['source_name']."]获得用户".$_user['nick']."打赏。");
            $return['code'] = 0;
            $return['msg'] = "礼物打赏成功";
        }
        return $return;
    }
}