﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{


    public partial class index : Form
    {
        DAL.index fi = new DAL.index();
        List<word> 下载队列 = new List<word>();
        public index()
        {
            InitializeComponent();
        }


        private void index_Load(object sender, EventArgs e)
        {
            this.dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;//选择一行
            this.dataGridView1.RowHeadersVisible = false;//隐藏第一列
            this.dataGridView1.AllowUserToResizeRows = false;//禁止高度
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;// 禁止用户改变列头的高度
            this.textBox2.Text = "a" + new Random().Next(99999);
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            //下载更新
            this.timer1.Enabled = false;

            刷新gv();
            总进度();
            this.timer1.Enabled = true;


        }

        private void 总进度()
        {
            this.label4.Text = string.Format("进度：{0}% | 速度：{1}", fi.返回总进度(), fi.返回总下载速度_简称());
        }

        public void 刷新gv()
        {
            for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
            {
                var guid = dataGridView1["guid", i].Value;
                if (guid == null) { continue; }
                var a = fi.查询任务信息(guid.ToString());
                if (a == null) { continue; }
                dataGridView1["进度", i].Value = a.进度 + "%";
                dataGridView1["速度", i].Value = a.下载速度_简文;
                dataGridView1["文件大小", i].Value = a.下载了多少_简文 + "/" + a.文件总大小_简文;
                dataGridView1["剩余时间", i].Value = a.剩余时间;
            }
        }

        /// <summary>
        /// 刷新下载列表
        /// </summary>
        public void sx()
        {
            dataGridView1.DataSource = new BindingList<word>(new List<word>());
            this.dataGridView1.DataSource = 下载队列;
        }



        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "下载到哪里？";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string foldPath = dialog.SelectedPath;
                this.textBox1.Text = foldPath;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //添加下载
            string url = this.textBox3.Text.Trim();
            string fileName = this.textBox2.Text.Trim();
            if (url == "" || fileName == "") { MessageBox.Show("文件名与下载地址不能为空"); return; }

            var q = 下载队列.Where(p => p.url == url || p.fileName == fileName).ToArray();
            if (q.Length > 0) { MessageBox.Show("此任务已经存在"); return; }


            word wd = new word();
            wd.url = url;
            wd.fileName = fileName;
            wd.guid = fi.下载(url, this.textBox1.Text + @"\", fileName);
            下载队列.Add(wd);
            sx();
            this.textBox2.Text = "a" + new Random().Next(99999);


        }

        private void 取消下载ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try { int xz = dataGridView1.CurrentRow.Index; }
            catch (Exception) { MessageBox.Show("请选择要取消的项"); return; }


            DialogResult dr = MessageBox.Show("正在下载的也会一起移除,确定操作吗？", "确认要移除吗", MessageBoxButtons.OKCancel);
            if (dr == DialogResult.OK)//如果点击“确定”按钮
            {
                string guid = dataGridView1["guid", dataGridView1.CurrentRow.Index].Value.ToString();
                DAL.model downm = fi.查询任务信息(guid);
                if (downm != null)
                {
                    fi.取消下载(guid, true);
                }
                下载队列.RemoveAll(p => p.guid == guid);
                sx();
            }
        }
    }
}
